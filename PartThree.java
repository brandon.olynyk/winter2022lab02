import java.util.Scanner;
public class PartThree
{
	
	public static void main(String[]args)
	{
		AreaComputations ac = new AreaComputations();		
		Scanner scan = new Scanner(System.in);
		
		//get values from user
		System.out.println("Please enter the side length of a square.");
		double squareSide = scan.nextDouble();
		
		System.out.println("Please enter the width of a rectangle.");
		double rectangleWidth = scan.nextDouble();
		
		System.out.println("Please enter the length of a rectangle.");
		double rectangleLength = scan.nextDouble();
		
		//calculate
		double squareArea = ac.areaSquare(squareSide);
		double rectangleArea = ac.areaRectangle(rectangleLength, rectangleWidth);
		
		//print final answers
		System.out.println("The area of the square is: " + squareArea);
		System.out.println("The area of the rectangle is: " + rectangleArea);
	}
}