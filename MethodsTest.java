public class MethodsTest
{
 public static void main(String[] args)
 {
  int x = 10;
  System.out.println("x before the method is called is: " + x);
  methodNoInputNoReturn();
  System.out.println("x after the method is called is: " + x); 
  methodOneInputNoReturn(10);
  methodOneInputNoReturn(x);
  methodOneInputNoReturn(x + 50); 
  methodTwoInputNoReturn(39, 2.4); 
  int z = methodNoInputReturnInt();
  System.out.println("z (set as methodNoInputReturnInt) has the value of: " + z); 
  double squareRootResult = sumSquareRoot(6, 3);
  System.out.println("The result of the sumSquareRoot operation is: " + squareRootResult);
  String s1 = "hello";
  String s2 = "goodbye";
  System.out.println("s1 length: " + s1.length());
  System.out.println("s1 length: " + s2.length()); 
  SecondClass sc = new SecondClass();
  System.out.println("addOne = " + SecondClass.addOne(50));
  System.out.println("addTwo = " + sc.addTwo(50));
 }
 
 public static void methodNoInputNoReturn()
 {
  int x = 50;
  System.out.println("I'm in a method that takes no input and returns nothing");
  System.out.println("x in the method is: " + x);
 }

 public static void methodOneInputNoReturn(int input)
 {
  System.out.println("Inside the method one input no return " + input);
  
 }
 
  public static void methodTwoInputNoReturn(int inputInt, double inputDouble)
 {
  System.out.println("Inside the method two input no return. Int input: " + inputInt + ", Input Double: " + inputDouble);
  
 }
  
  public static int methodNoInputReturnInt()
  {
    return 6;
  }
  
  public static double sumSquareRoot(int input1, int input2)
  {
    double result = input1 + input2;
    Math.sqrt(result);
    return result;
  }
}