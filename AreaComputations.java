public class AreaComputations
{
	public static double areaSquare(double sideLength)
	{
		//this method gets the length of a side of a square and returns the square's area
		double finalAnswer = sideLength * sideLength;
		return finalAnswer;
	}
	
	public double areaRectangle(double length, double width)
	{
		double finalAnswer = length * width;
		return finalAnswer;
	}
}